﻿using System.Linq;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

// ReSharper disable HeapView.ObjectAllocation
// ReSharper disable HeapView.DelegateAllocation

namespace Instrumenta.Utils
{
    public static class AssetDatabaseUtil
    {
        [PublicAPI]
        public static T LoadAssetOfType<T>() where T : Object
        {
            var assets = LoadAssetsOfType<T>();

            return assets == null || assets.Length == 0 ? null : assets[0];
        }

        [PublicAPI]
        public static T[] LoadAssetsOfType<T>() where T : Object
        {
            var guids = AssetDatabase.FindAssets($"t:{typeof(T).Name}");

            if (guids.Length == 0)
            {
                return null;
            }

            var assets = guids
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<T>)
                .ToArray();

            return assets;
        }

        [PublicAPI]
        public static string GetAssetAbsolutePath(Object folderAsset)
        {
            return $"{Application.dataPath}/../{AssetDatabase.GetAssetPath(folderAsset)}";
        }
    }
}