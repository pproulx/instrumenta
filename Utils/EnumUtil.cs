﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

// ReSharper disable HeapView.PossibleBoxingAllocation
// ReSharper disable HeapView.ObjectAllocation.Possible

namespace Instrumenta.Utils
{
    public static class EnumUtil
    {
        [PublicAPI]
        public static IEnumerable<T> GetFlags<T>(T input) where T : Enum
        {
            foreach (T value in Enum.GetValues(input.GetType()))
            {
                if (input.HasFlag(value))
                {
                    yield return value;
                }
            }
        }
    }
}