using System;

namespace Instrumenta.Utils
{
    public static class CodeGenerationUtil
    {
        public static string ConvertParametersToArguments(string parameters)
        {
            // ReSharper disable once HeapView.ObjectAllocation
            var parameterArray = parameters.Split(',');

            for (var i = 0; i < parameterArray.Length; i++)
            {
                var parameter = parameterArray[i].Trim();

                if (parameter != string.Empty)
                {
                    var spaceIndex = parameter.IndexOf(" ", StringComparison.OrdinalIgnoreCase);
                    var argument = parameter.Substring(spaceIndex).Trim();

                    parameterArray[i] = argument;
                }
            }

            return string.Join(", ", parameterArray);
        }
    }
}