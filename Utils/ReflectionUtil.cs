﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

// ReSharper disable HeapView.ObjectAllocation

namespace Instrumenta.Utils
{
    public static class ReflectionUtil
    {
        public static List<T> GetConstantValues<T>(Type type)
        {
            var fields = type.GetFields(
                BindingFlags.Public |
                BindingFlags.Static |
                BindingFlags.FlattenHierarchy);

            return fields.Where(fieldInfo =>
                fieldInfo.IsLiteral &&
                !fieldInfo.IsInitOnly &&
                fieldInfo.FieldType == typeof(T)).Select(fi => (T) fi.GetRawConstantValue()).ToList();
        }
    }
}