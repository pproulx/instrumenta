﻿using System;

namespace Instrumenta.Utils
{
    public static class StringUtil
    {
        public static int IndexOf(string text, string search, int startIndex,
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase)
        {
            return text.IndexOf(search, startIndex, comparisonType);
        }

        public static int IndexOfAfter(string text, string search, int startIndex,
            StringComparison comparisonType = StringComparison.OrdinalIgnoreCase)
        {
            return IndexOf(text, search, startIndex, comparisonType) + search.Length;
        }
    }
}