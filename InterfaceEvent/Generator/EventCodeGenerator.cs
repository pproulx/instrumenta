using System;
using System.IO;
using Instrumenta.Utils;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Instrumenta.InterfaceEvent
{
    [CreateAssetMenu(fileName = "EventCodeGenerator", menuName = "ScriptableObjects/EventCodeGenerator", order = 1)]
    public class EventCodeGenerator : ScriptableObject
    {
        private const string FileSuffix = ".cs";
        private const string GeneratedFilePrefix = "Event";
        private const string GeneratedFileSuffix = ".generated.cs";
        private const string PublicInterfaceKeywords = "public interface";
        private const string InterfaceNamePrefix = "IEvent";
        private const string ParametersPrefix = "(";
        private const string ParametersSuffix = ")";
        private const string TemplatePlaceholderName = "Template";
        private const string TemplatePlaceholderParameters = "/* parameters */";
        private const string TemplatePlaceholderArguments = "/* arguments */";

        private static readonly string _TemplateIssueToRemove =
            $"{Environment.NewLine}// ReSharper disable UnusedType.Global{Environment.NewLine}// ReSharper disable UnusedMember.Global{Environment.NewLine}// @formatter:off{Environment.NewLine}";

        [SerializeField]
        private TextAsset EventClassTemplate;

        [SerializeField]
        private Object EventClassesInputDirectory;
        
        [SerializeField]
        private Object EventClassesOutputDirectory;

        private string OutputDirectoryPath => AssetDatabaseUtil.GetAssetAbsolutePath(EventClassesOutputDirectory);
        private string InputDirectoryPath => AssetDatabaseUtil.GetAssetAbsolutePath(EventClassesInputDirectory);

        [MenuItem(nameof(Instrumenta) + "/Generate event classes %#e")]
        private static void GenerateEventClasses()
        {
            var eventCodeGenerators = AssetDatabaseUtil.LoadAssetsOfType<EventCodeGenerator>();
            
            foreach (var eventCodeGenerator in eventCodeGenerators)
            {
                eventCodeGenerator.GenerateEventClassesFromDefinitions();
            }
        }

        private void GenerateEventClassesFromDefinitions()
        {
            PrepareDirectory();
            
            foreach (var filePath in Directory.EnumerateFiles(InputDirectoryPath, $"{InterfaceNamePrefix}*{FileSuffix}", SearchOption.AllDirectories))
            {
                if (filePath.EndsWith($"{InterfaceNamePrefix}{TemplatePlaceholderName}{FileSuffix}"))
                {
                    continue;
                }
                
                var fileContent = File.ReadAllText(filePath);
                
                GenerateEventClassesFromDefinition(fileContent);
            }

            AssetDatabase.Refresh();

            // It seems that this is not working when EditorSettings.enterPlayModeOptionsEnabled is true:
            CompilationPipeline.RequestScriptCompilation();

            var thisAssetPath = AssetDatabase.GetAssetPath(this);
            Debug.Log($"Event code generation completed for {thisAssetPath}", this);
        }

        private void GenerateEventClassesFromDefinition(string eventDefinition)
        {
            var startIndex = 0;

            do
            {
                startIndex = StringUtil.IndexOf(eventDefinition, PublicInterfaceKeywords, startIndex + 1);

                if (startIndex > 0)
                {
                    GenerateEventClassFromDefinition(eventDefinition, startIndex + PublicInterfaceKeywords.Length);
                }
            } while (startIndex > 0);
        }

        private void PrepareDirectory()
        {
            Directory.CreateDirectory(OutputDirectoryPath);

            var directoryInfo = new DirectoryInfo(OutputDirectoryPath);

            foreach (var fileInfo in directoryInfo.GetFiles())
            {
                if (fileInfo.Name.EndsWith(GeneratedFileSuffix))
                {
                    fileInfo.Delete();
                }
            }
        }

        private void GenerateEventClassFromDefinition(string eventDefinition, int startIndex)
        {
            var eventNameStartIndex = StringUtil.IndexOfAfter(eventDefinition, InterfaceNamePrefix, startIndex);
            var eventNameEndIndex = StringUtil.IndexOf(eventDefinition, " ", eventNameStartIndex);
            var eventName = eventDefinition.Substring(eventNameStartIndex, eventNameEndIndex - eventNameStartIndex);

            var eventParametersStartIndex = StringUtil.IndexOfAfter(eventDefinition, ParametersPrefix, startIndex);
            var eventParametersEndIndex = StringUtil.IndexOf(eventDefinition, ParametersSuffix, eventParametersStartIndex);
            var eventParameters = eventDefinition.Substring(eventParametersStartIndex, eventParametersEndIndex - eventParametersStartIndex);

            var eventArguments = CodeGenerationUtil.ConvertParametersToArguments(eventParameters);

            var eventClassTemplate = EventClassTemplate.text;

            var eventClassCode = eventClassTemplate
                .Replace(TemplatePlaceholderName, eventName)
                .Replace(TemplatePlaceholderParameters, eventParameters)
                .Replace(TemplatePlaceholderArguments, eventArguments)
                .Replace(_TemplateIssueToRemove, string.Empty);

            WriteEventClassFile(eventName, eventClassCode);
        }

        private void WriteEventClassFile(string eventName, string eventClassCode)
        {
            var filePath = $"{OutputDirectoryPath}/{GeneratedFilePrefix}{eventName}{GeneratedFileSuffix}";

            TextWriter textWriter = File.CreateText(filePath);
            textWriter.Write(eventClassCode);
            textWriter.Close();
        }
    }
}