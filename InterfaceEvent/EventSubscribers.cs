﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Instrumenta.InterfaceEvent
{
    public class EventSubscribers
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void Initialize() => _Instance = null;

        private Dictionary<Type, List<IEventBase>> _SubscribersByType;
        private Dictionary<Type, List<IEventBase>> SubscribersByType => _SubscribersByType ??= new Dictionary<Type, List<IEventBase>>();

        private static EventSubscribers _Instance;
        private static EventSubscribers Instance => _Instance ??= new EventSubscribers();

        public static void Subscribe<T>(T subscriber) where T : class, IEventBase
        {
            var type = typeof(T);
            var subscribersByType = Instance.SubscribersByType;

            if (!subscribersByType.TryGetValue(type, out var subscribers))
            {
                subscribers = new List<IEventBase>();
                subscribersByType.Add(type, subscribers);
            }

            subscribers.Add(subscriber);
        }

        public static void Unsubscribe<T>(T subscriber) where T : class, IEventBase
        {
            var type = typeof(T);
            var subscribersByType = Instance.SubscribersByType;

            if (subscribersByType.TryGetValue(type, out var subscribers))
            {
                subscribers.Remove(subscriber);
            }
        }

        // ReSharper disable once ReturnTypeCanBeEnumerable.Global
        public static List<IEventBase> Get<T>() where T : IEventBase
        {
            var type = typeof(T);
            var subscribersByType = Instance.SubscribersByType;

            if (subscribersByType.TryGetValue(type, out var subscribers))
            {
                return subscribers;
            }

            var newSubscribers = new List<IEventBase>();
            subscribersByType.Add(type, newSubscribers);
            return newSubscribers;
        }
    }
}