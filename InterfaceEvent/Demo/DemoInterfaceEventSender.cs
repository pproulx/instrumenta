﻿using UnityEngine;

namespace Instrumenta.InterfaceEvent.Demo
{
    public class DemoInterfaceEventSender : MonoBehaviour
    {
        private void Update()
        {
            if (Input.anyKeyDown)
            {
                SendEvents();
            }
        }

        private static void SendEvents()
        {
            EventLevelStart.Invoke();
            
            EventLevelEnd.Invoke();
            EventLevelEnd.Invoke();
            
            EventCharacterHealthChange.Invoke(50);
        }
    }
}