﻿namespace Instrumenta.InterfaceEvent.Demo
{
    public interface IEventCharacterHealthChange : IEventBase
    {
        void OnReceive(float newHealth);
    }
}