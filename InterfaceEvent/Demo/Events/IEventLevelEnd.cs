﻿namespace Instrumenta.InterfaceEvent.Demo
{
    public interface IEventLevelEnd : IEventBase
    {
        void OnReceive();
    }
}