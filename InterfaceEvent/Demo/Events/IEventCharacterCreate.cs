namespace Instrumenta.InterfaceEvent.Demo
{
    public interface IEventCharacterCreate : IEventBase
    {
        void OnReceive(float minimum, float maximum);
    }
}