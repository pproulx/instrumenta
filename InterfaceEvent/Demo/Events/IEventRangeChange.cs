namespace Instrumenta.InterfaceEvent.Demo
{
    public interface IEventRangeChange : IEventBase
    {
        void OnReceive(float minimum, float maximum);
    }
}