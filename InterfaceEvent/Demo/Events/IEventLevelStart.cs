﻿namespace Instrumenta.InterfaceEvent.Demo
{
    public interface IEventLevelStart : IEventBase
    {
        void OnReceive();
    }
}