﻿using System.Globalization;
using UnityEngine;

namespace Instrumenta.InterfaceEvent.Demo
{
    public class DemoInterfaceEventReceiver : MonoBehaviour, IEventLevelStart, IEventLevelEnd, IEventCharacterHealthChange
    {
        private void OnEnable()
        {
            EventLevelStart.Subscribe(this);
            EventLevelEnd.Subscribe(this);
            EventCharacterHealthChange.Subscribe(this);
        }

        private void OnDisable()
        {
            EventLevelStart.Unsubscribe(this);
            EventLevelEnd.Unsubscribe(this);
            EventCharacterHealthChange.Unsubscribe(this);
        }

        void IEventLevelStart.OnReceive()
        {
            Debug.Log($"{nameof(IEventLevelStart)}");
        }

        void IEventLevelEnd.OnReceive()
        {
            Debug.Log($"{nameof(IEventLevelEnd)}");
        }
        
        void IEventCharacterHealthChange.OnReceive(float newHealth)
        {
            Debug.Log($"{nameof(IEventCharacterHealthChange)} {newHealth.ToString(CultureInfo.InvariantCulture)}");
        }
    }
}