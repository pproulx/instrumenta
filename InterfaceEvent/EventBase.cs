using JetBrains.Annotations;

namespace Instrumenta.InterfaceEvent
{
    public interface IEventBase
    {
    }

    public class EventBase<T> where T : class, IEventBase
    {
        [PublicAPI]
        public static void Subscribe(T subscriber)
        {
            EventSubscribers.Subscribe(subscriber);
        }

        [PublicAPI]
        public static void Unsubscribe(T subscriber)
        {
            EventSubscribers.Unsubscribe(subscriber);
        }
    }
}