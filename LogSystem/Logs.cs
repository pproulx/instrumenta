﻿using System.Collections.Generic;
using Instrumenta.Utils;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

// ReSharper disable HeapView.DelegateAllocation

namespace Instrumenta.LogSystem
{
    public static class Logs
    {
        private static readonly List<string> _Labels;
        public static readonly List<string> Labels = _Labels ??= ReflectionUtil.GetConstantValues<string>(typeof(LogLabel));

        private static Dictionary<string, bool> _LabelsEnableFromLabel;
        public static Dictionary<string, bool> LabelsEnableFromLabel => _LabelsEnableFromLabel ??= InitializeLabelsEnable();

        public static string LabelKey(string label) => $"{nameof(Instrumenta)}-{nameof(LogSystem)}-{label}-Enable";

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void Reset()
        {
            _LabelsEnableFromLabel = null;
        }

        private static Dictionary<string, bool> InitializeLabelsEnable()
        {
            var newLabelsEnableFromLabel = new Dictionary<string, bool>(Labels.Count);

            foreach (var label in Labels)
            {
                var labelKey = LabelKey(label);
                var labelEnable = EditorPrefs.GetBool(labelKey, true);

                newLabelsEnableFromLabel[label] = labelEnable;
            }

            return newLabelsEnableFromLabel;
        }

        private static string GetLabelText(string logLabel, object message) => $"<b>[<color=#AACCFF>{logLabel}</color>]</b> {message}";

        private static bool IsLabelEnable(string label) => LabelsEnableFromLabel.TryGetValue(label, out var enable) && enable;

        [PublicAPI]
        public static void Log(object message, Object context = null)
        {
            Log(LogLabel.Debug, message, Debug.Log, context);
        }

        [PublicAPI]
        public static void Log(string logLabel, object message, Object context = null)
        {
            Log(logLabel, message, Debug.Log, context);
        }

        [PublicAPI]
        public static void LogWarning(object message, Object context = null)
        {
            Log(LogLabel.Debug, message, Debug.LogWarning, context);
        }

        [PublicAPI]
        public static void LogWarning(string logLabel, object message, Object context = null)
        {
            Log(logLabel, message, Debug.LogWarning, context);
        }

        [PublicAPI]
        public static void LogError(object message, Object context = null)
        {
            Log(LogLabel.Debug, message, Debug.LogError, context);
        }

        [PublicAPI]
        public static void LogError(string logLabel, object message, Object context = null)
        {
            Log(logLabel, message, Debug.LogError, context);
        }

        [PublicAPI]
        public static void Log(string logLabel, object message, System.Action<object, Object> logMethod, Object context = null)
        {
            if (!IsLabelEnable(logLabel))
            {
                return;
            }

            var messageText = GetLabelText(logLabel, message);

            logMethod(messageText, context);
        }
    }
}