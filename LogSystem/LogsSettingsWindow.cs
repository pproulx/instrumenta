using UnityEditor;
using UnityEngine;

namespace Instrumenta.LogSystem
{
    public class LogsSettingsWindow : EditorWindow
    {
        private Vector2 _ScrollPosition;

        // ReSharper disable once Unity.DuplicateShortcut
        [MenuItem(nameof(Instrumenta) + "/Open log filters")]
        public static void ShowWindow()
        {
            var window = GetWindow(typeof(LogsSettingsWindow));
            window.titleContent = new GUIContent("Log Filter");
            window.minSize = new Vector2(window.minSize.x + 16, window.minSize.y);
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            HandleButtonAll();
            HandleButtonNone();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical();
            // ReSharper disable once HeapView.ObjectAllocation
            _ScrollPosition = EditorGUILayout.BeginScrollView(_ScrollPosition, GUILayout.Width(EditorGUIUtility.currentViewWidth - 2));

            foreach (var label in Logs.Labels)
            {
                HandleButtonLabel(label);
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }

        private static void HandleButtonAll()
        {
            if (GUILayout.Button("All", EditorStyles.toolbarButton))
            {
                SetLabelsEnable(true);
            }
        }

        private static void HandleButtonNone()
        {
            if (GUILayout.Button("None", EditorStyles.toolbarButton))
            {
                SetLabelsEnable(false);
            }
        }

        private static void HandleButtonLabel(string label)
        {
            var labelEnable = Logs.LabelsEnableFromLabel[label];

            var buttonText = ObjectNames.NicifyVariableName(label);
            var newLabelEnable = GUILayout.Toggle(labelEnable, buttonText);

            if (newLabelEnable != labelEnable)
            {
                SetLabelEnable(label, newLabelEnable);
            }
        }

        private static void SetLabelEnable(string label, bool enable)
        {
            var labelKey = Logs.LabelKey(label);

            EditorPrefs.SetBool(labelKey, enable);
            Logs.LabelsEnableFromLabel[label] = enable;
        }

        private static void SetLabelsEnable(bool enable)
        {
            foreach (var label in Logs.Labels)
            {
                SetLabelEnable(label, enable);
            }
        }
    }
}