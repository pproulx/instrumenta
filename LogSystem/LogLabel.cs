﻿// ReSharper disable UnusedMember.Global

namespace Instrumenta.LogSystem
{
    public static class LogLabel
    {
        public const string Debug = "Debug";
        public const string UserInterface = "UserInterface";
        public const string Network = "Network";
        public const string GameFlow = "GameFlow";
        public const string Character = "Character";
        public const string Engine = "Engine";
    }
}